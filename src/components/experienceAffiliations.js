export class experienceAffiliations extends HTMLElement {

  constructor() {
    super();
    this.template();

    this.affiliationsData = this.querySelector(".affiliations");
    this.container = this.querySelector('.exp-affiliations');
    this.container.style.display = "none";
  }

  set affiliations(data) {
    this._affiliations = data;
    if (data) {
      this.affiliationsData.innerHTML = data.map(affiliationsData => `
        <li>
          <a href="#">
            <img src="${affiliationsData.logoUrl}" alt="exper-logo">
          </a>
        </li>
      `).join('');
      this.container.style.display = "block";
    } else {
      this.affiliationsData.innerHTML = "";
      this.container.style.display = "none";
    }
  }

  template() {
    this.innerHTML = `
      <div class="exp-affiliations">
        <div class="container">
          <p class="title">Affiliations</p>

          <div class="experience-logo">
            <ul class="list-logo affiliations"></ul>
          </div>

          <div class="map-detail" style="display: none;">
            <p class="address">Dali, Yunnan, China</p>
            <div class="map"></div>
          </div>
        </div>
      </div>
    `;
  }

}