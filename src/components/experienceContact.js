import contact01 from '../assets/images/contact-01.png';
import contact02 from '../assets/images/contact-02.png';
import contact03 from '../assets/images/contact-03.png';
import contact04 from '../assets/images/contact-04.png';
import contact05 from '../assets/images/contact-05.png';
export class experienceContact extends HTMLElement {

  constructor() {
    super();
    this.template();
    this.addresData = this.querySelector(".address-data");
    this.telephoneData = this.querySelector(".telephone-data");
    this.emailData = this.querySelector(".email-data");
    this.websiteData = this.querySelector(".website-data");
    this.what3wordsData = this.querySelector(".what3words-data");

    this.addressContainer = this.querySelector("li.address");
    this.telephoneContainer = this.querySelector("li.telephone");
    this.emailContainer = this.querySelector("li.email");
    this.websiteContainer = this.querySelector("li.website");
    this.what3wordsContainer = this.querySelector("li.what3words");

    this.addressContainer.style.display = "none";
    this.telephoneContainer.style.display = "none";
    this.emailContainer.style.display = "none";
    this.websiteContainer.style.display = "none";
    this.what3wordsContainer.style.display = "none";
  }

  set address(address) {
    this._address = address;
    if (address) {
      this.addresData.innerHTML = this._address;
      this.addressContainer.style.display = "flex";
    } else {
      this.addressContainer.style.display = "none";
    }
  }

  get address() {
    return this._address;
  }

  set telephone(tel) {
    this._telephone = tel;
    if (tel) {
      this.telephoneData.innerHTML = this._telephone;
      this.telephoneContainer.style.display = "flex";
    } else {
      this.telephoneData.innerHTML = "";
      this.telephoneContainer.style.display = "none";
    }
  }

  set email(email) {
    this._email = email;
    if (email) {
      this.emailData.innerHTML = this._email;
      this.emailContainer.style.display = "flex";
    } else {
      this.emailData.innerHTML = "";
      this.emailContainer.style.display = "none";
    }
  }

  set website(data) {
    this._website = data;
    if (data) {
      this.websiteData.innerHTML = `<a href="${data.link}" target="_blank" rel="noopener noreferrer">${data.name}</a>`;
      this.websiteContainer.style.display = "flex";
    } else {
      this.websiteData.innerHTML = "";
      this.websiteContainer.style.display = "none";
    }
  }

  set what3words(data) {
    this._what3words = data;
    if (data) {
      this.what3wordsData.innerHTML = data;
      this.what3wordsContainer.style.display = "flex";
    } else {
      this.what3wordsData.innerHTML = "";
      this.what3wordsContainer.style.display = "none";
    }
  }

  template() {
    this.innerHTML = `
      <div class="exp-contact">
        <ul class="list-contact">
          <li class="address">
            <div class="icon">
              <img src="${contact01}" alt="icon-contact">
            </div>
            <div class="post-contact address-data">${this._address}</div>
          </li>
          <li class="telephone">
            <div class="icon"><img src="${contact02}" alt="icon-contact"></div>
            <div class="post-contact telephone-data"></div>
          </li>
          <li class="email">
            <div class="icon"><img src="${contact03}" alt="icon-contact"></div>
            <div class="post-contact email-data"></div>
          </li>
          <li class="website">
            <div class="icon"><img src="${contact04}" alt="icon-contact"></div>
            <div class="post-contact website-data"></div>
          </li>
          <li class="what3words">
            <div class="icon"><img src="${contact05}" alt="icon-contact"></div>
            <div class="post-contact what3words-data"></div>
          </li>
        </ul>
      </div>
    `;
  }

}

