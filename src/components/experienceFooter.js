export class experienceFooter extends HTMLElement {

  constructor() {
    super();
    this._privacyLink = "#";
    this._termsLink = "#";
    this._copyrights = `© ${new Date().getFullYear()} Chameleon Strategies. All rights reserved.`;
    this.template();
  }

  get copyrights() {
    return this._copyrights;
  }

  set copyrights(link) {
    this._copyrights = link;
    this.template();
  }

  get privacyLink() {
    return this._privacyLink;
  }

  set privacyLink(link) {
    this._privacyLink = link;
    this.template();
  }

  get termsLink() {
    return this._termsLink;
  }

  set termsLink(link) {
    this._termsLink = link;
    this.template();
  }

  template() {
    this.innerHTML = `
      <div class="exp-footer">
        <div class="container">
          <div class="detail-footer">
            <div class="reserved">
              <p>${this._copyrights}</p>
            </div>
            <div class="footer-link">
              <ul class="links">
                <li><a href="${this._privacyLink}">Terms & Conditions</a></li>
                <li><a href="${this._privacyLink}">Privacy</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    `;
  }

}